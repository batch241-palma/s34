/* Activity S33 */

const express = require("express");

const app = express(); 

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

/* No.1 & 2*/

app.get("/home", (request, response) => {
	 
	response.send("Welcome to the home page");
});


/* No. 3 & 4*/
let users = [{
	"username": "johndoe",
	"password": "johndoe1234"
}];

app.get("/users", (request, response) => {
	 
	response.send(users);
});


/* register (Optional if you want to add another data) */


app.post("/register", (request, response) => {

    if(request.body.username !== '' && request.body.password !== ''){

        users.push(request.body);

        response.send(`User ${request.body.username} successfully registered!`);

    } else {

        response.send("Please input BOTH username and password.");

    }

});

app.get("/users", (request, response) => {
	 
	response.send(users);
});


/* No. 5 & 6*/

app.delete("/delete-user", (request, response) => {


		for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username) {
			users.splice(users[i]) ;
		} else {
			message = "User does not exist.";
		}
	}
	response.send(`User ${request.body.username} has been deleted`);
});





app.listen(port, () => console.log(`Server running at port ${port}`));